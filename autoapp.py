# -*- coding: utf-8 -*-
"""Create an application instance."""
from fk.app import create_app

app = create_app()
