# -*- coding: utf-8 -*-
"""Application configuration.

Most configuration is set via environment variables.

For local development, use a .env file to set
environment variables.
"""
import os

from environs import Env

env = Env()
env.read_env()

ENV = env.str("FLASK_ENV", default="production")
DEBUG = ENV == "development"
SQLALCHEMY_DATABASE_URI = env.str("DATABASE_URL")
SECRET_KEY = env.str("SECRET_KEY")
SEND_FILE_MAX_AGE_DEFAULT = env.int("SEND_FILE_MAX_AGE_DEFAULT")
BCRYPT_LOG_ROUNDS = env.int("BCRYPT_LOG_ROUNDS", default=13)
DEBUG_TB_ENABLED = DEBUG
DEBUG_TB_INTERCEPT_REDIRECTS = False
CACHE_TYPE = "redis"  # defalut "simple" , Can be "memcached", "redis", etc.
SQLALCHEMY_TRACK_MODIFICATIONS = False
SQLALCHEMY_ECHO = True  # 输出SQL语句


# __file__ refers to the file settings.py
APP_ROOT = os.getcwd()  # 项目根目录
APP_STATIC = os.path.join(APP_ROOT, 'static')
UPLOAD_FOLDER = os.path.join(APP_ROOT, 'upload')
UPLOADED_PHOTOS_DEST = os.path.join(UPLOAD_FOLDER, 'photos')
UPLOADED_FILES_DEST = os.path.join(UPLOAD_FOLDER, 'files')
MAX_CONTENT_LENGTH = 10 * 1024 * 1024
