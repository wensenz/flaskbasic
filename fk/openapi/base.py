import json
import os
import time

from flask import Blueprint
from werkzeug.utils import secure_filename

from fk.settings import UPLOAD_FOLDER

blueprint = Blueprint("openapi", __name__, url_prefix="/api")


@blueprint.route("/")
def home():
    welcome = "welcome request open api"
    return welcome


def var_dump_json(data, code=0, msg=''):
    return json.dumps([{
        'code': code,
        'msg': msg,
        'data': data
    }])


ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


def upload_img(file):
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        path_and_name = os.path.join(UPLOAD_FOLDER, filename)
        file.save(path_and_name)  # 保存到配置目录
        return filename


# 重新定义上传文件的名称
def upload_name_re_def(filename):
    _, prefix = filename.split('.')
    final_name = time.strftime("%Y%m%d%H%M%S") + '.' + prefix
    return final_name
