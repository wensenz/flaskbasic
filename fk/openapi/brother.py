from flask import request, redirect, url_for

from fk.openapi.base import blueprint, var_dump_json, upload_img


@blueprint.route("/bro/", methods=["GET", "POST"])
def brother():
    # request.json_module.request.args
    distUser = {'id': request.args['id'], 'name': request.args.get('name', '')}
    return var_dump_json(distUser)


@blueprint.route('/get/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return 'User %s' % username


@blueprint.route('/jump/')
def jump():
    return redirect(url_for('openapi.hello'))


@blueprint.route('/hello/')
def hello():
    return 'hello redirect'
