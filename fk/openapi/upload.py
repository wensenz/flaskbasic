# -*- coding: utf-8 -*-
from flask import request, url_for, send_from_directory

from fk.extensions import photos, documents
from fk.openapi.base import blueprint, upload_img, upload_name_re_def
from fk.settings import UPLOAD_FOLDER

html = '''
    <!DOCTYPE html>
    <title>Upload File</title>
    <h1>图片上传</h1>
    <form method=post enctype=multipart/form-data>
         <input type=file name=file>
         <input type=submit value=上传>
    </form>
    '''

html2 = '''
    <!DOCTYPE html>
    <title>Upload File</title>
    <h1>Flask-Upload图片上传</h1>
    <form method=post enctype=multipart/form-data>
         <input type=file name=photo>
         <input type=submit value=上传>
    </form>
    '''


# 图片显示
@blueprint.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)


@blueprint.route('/upload/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        filename = upload_img(file)
        file_url = url_for('openapi.uploaded_file', filename=filename)
        return html + '<br><img src=' + file_url + '>'
    return html


@blueprint.route('/uploadPlus/', methods=['GET', 'POST'])
def flask_upload_photo():
    if request.method == 'POST' and 'photo' in request.files:
        filename = photos.save(request.files['photo'])
        file_url = photos.url(filename)
        return html2 + '<br><img src=' + file_url + '>'
    return html2


@blueprint.route('/uploadFile/', methods=['GET', 'POST'])
def flask_upload_files():
    if request.method == 'POST' and 'photo' in request.files:
        files = request.files['photo']
        upload_file_name = upload_name_re_def(files.filename)  # secure_filename会导致上传中文文件有问题,所以这里避免用中文名
        filename = documents.save(request.files['photo'], name=upload_file_name)
        file_url = documents.url(filename)
        return html2 + '<br><a href="' + file_url + '" download="' + filename + '">点击下载</a>'
    return html2
